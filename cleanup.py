"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import time
import os

from rpg.reader import read_dataclass, read_dir
from rpg.pretty_serializer import serialize_dataclass
from rpg.models import (
    Archetype,
    Armor,
    BaseClass,
    ClassChoices,
    Feat,
    Item,
    Race,
    Skill,
    Spell,
    Weapon,
)


data_types = [
    Archetype,
    Armor,
    BaseClass,
    Feat,
    Item,
    Race,
    Skill,
    Spell,
    Weapon,
]

class_choices = {
    'domains': 'domains',
    'blessings': 'warpriest-blessings',
}

def main():
    parser = argparse.ArgumentParser(description="Generate the RPG Rules site.")
    for data_type in data_types:
        parser.add_argument(f'--{data_type.__name__.lower()}', type=str, default=None)
    for arg, _ in class_choices.items():
        parser.add_argument(f'--{arg}', type=str, default=None)
    args = parser.parse_args()
    clean_data(args)


def clean_data(args):
    for data_type in data_types:
        value = getattr(args, data_type.__name__.lower())
        if not value:
            continue

        if value == "*":
            for data in read_dir(data_type):
                serialize_dataclass(data)
        else:
            for id_to_clean in value.split(','):
                data = read_dataclass(data_type, id_to_clean)
                serialize_dataclass(data)

    for arg, directory in class_choices.items():
        value = getattr(args, arg)
        directory = os.path.join('data', directory)
        if not value:
            continue

        if value == "*":
            for data in read_dir(ClassChoices, directory):
                serialize_dataclass(data, directory)
        else:
            for id_to_clean in value.split(','):
                data = read_dataclass(ClassChoices, id_to_clean, directory)
                serialize_dataclass(data, directory)

if __name__ == "__main__":
    start = time.perf_counter()
    main()
    end = time.perf_counter()

    print(f'Generated site in {end-start} seconds')
