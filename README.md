# RPG's Rule!

This repo contains the files and code used to generate my rpg rules site. The data is stored as yaml files in this repo.

## Dev setup.

For a development IDE, I recommend [VSCodium](https://vscodium.com/)

You will also need [python](https://www.python.org/downloads/) installed for your OS. This project requires at least version 3.8.

To install the dependencies, run `pip install -r requirements.txt`

To generate the html files, run `python .\main.py`

The files are saved to the public folder. Once the files are generated, you can try to load them in your browser, but will likely notice that they are missing the styling.

So the best way to serve the files locally is to run `python -m http.server -d ./public/`