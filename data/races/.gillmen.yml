name: Gillmen
sources:
  - 'Pathfinder Roleplaying Game Advanced Race Guide'
tags:
  - Aquatic
  - Uncommon
summary: 'Survivors of a land-dwelling culture whose homeland was destroyed, gillmen were saved and transformed into an amphibious race by the aboleths. Though in many ways they appear nearly human, gillmen''s bright purple eyes and gills set them apart from humanity. Reclusive and suspicious, gillmen know that one day the aboleths will call in the debt owed to them.'
ability_scores: '+2 Con, +2 Cha, --2 Wis'
type: humanoid
subtypes:
  - aquatic
size: Medium
ages:
  adulthood: 20
  intuitive: +1d6
  self_taught: +2d6
  trained: +3d6
  middle_age: 62
  old: 93
  venerable: 125
  maximum: +3d20
height_and_weight:
  height_modifier: +2d10
  weight_modifier: ×5
  genders:
    -
      name: male
      base_height: '4''10"'
      base_weight: '120 lb.'
    -
      name: female
      base_height: '4''5"'
      base_weight: '85 lb.'
text: "Gillmen are the remnants of a race of surface-dwelling humanoids whose homeland was drowned in a great cataclysm at the hands of the aboleth. The aboleths rescued a few survivors, warping them into an amphibious race to serve as emissaries to the surface world. Modern gillmen remain reclusive and suspicious, scarred by both the loss of their ancient heritage and the sure knowledge that aboleths do nothing without expecting to profit from it. Physically gillmen have expressive brows, pale skin, dark hair, and bright purple eyes. Three slim gills mark each side of their necks, near the shoulder, but they are otherwise close enough in appearance to humans that they can pass as such (for a time) without fear of detection.\n\n## Gillman Racial Traits\n\n- **+2 Constitution, +2 Charisma, --2 Wisdom**: Gillmen are vigorous and beautiful, but their domination by the aboleths has made them weak-willed.\n- **Medium:** Gillmen are Medium creatures and have no bonuses or penalties due to their size.\n- **Aquatic:** Gillmen are humanoids with the aquatic subtype.\n- **Normal Speed:** Gillmen have a base speed of 30 feet on land. As aquatic creatures, they also have a swim speed of 30 feet, can move in water without making [Swim](/skills/swim.html) checks, and always treat [Swim](/skills/swim.html) as a class skill.\n- **Amphibious:** Gillmen have the aquatic subtype, but can breathe both water and air.\n- **Enchantment Resistance:** Gillmen gain a +2 racial saving throw bonus against non-aboleth enchantment spells and effects, but take a --2 penalty on such saving throws against aboleth sources.\n- **Water Dependent:** A gillman's body requires constant submersion in fresh or salt water. Gillmen who spend more than 1 day without fully submerging themselves in water risk internal organ failure, painful cracking of the skin, and death within 4d6 hours.\n- **Languages:** Gillmen begin play speaking Common and Aboleth. Gillmen with high Intelligence scores can choose from the following: Aklo, Aquan, Draconic, and Elven.\n\n## Alternate Racial Traits\n\nThe following racial traits may be selected instead of existing gillman racial traits. Consult your GM before selecting any of these new options.\n\n**Riverfolk:** Some gillmen groups live in colonies along vast riverways, and have adapted to living on land for much longer periods. Gillmen with this trait have a thin coating of natural oil that keeps their skin from cracking even without water. However, this natural oil also makes such gillmen particularly susceptible to flames, and they gain vulnerability to fire. This racial trait replaces water dependent.\n\n**Slimehunter:** Gillmen with this trait are from lineages that have fought against aboleths since the aberrations rescued their human ancestors. They receive a +2 racial bonus on saving throws against aboleth spells, spell-like abilities, and supernatural abilities. This racial trait replaces enchantment resistance.\n\n**Throwback:** Some gillmen are throwbacks to their land-dwelling human ancestors. Gillmen with this racial trait do not have the amphibious trait, have the human subtype instead of the aquatic subtype, have no swim speed or bonuses to the [Swim](/skills/swim.html) skill, cannot breathe water, and do not have the water dependent racial trait.\n\n## Favored Class Options\n\nThe following options are available to all gillmen who have the listed favored class, and unless otherwise stated, the bonus applies each time you select the favored class reward.\n\n**Fighter:** Add +1 to the fighter's CMD when resisting two combat maneuvers of the character's choice.\n\n**Rogue:** The rogue gains 1/6 of a new rogue talent.\n\n**Sorcerer:** Add one spell known from the sorcerer spell list. This spell must be at least one level below the highest spell level the sorcerer can cast.\n\n**Wizard:** Add one spell from the wizard spell list to the wizard's spellbook. This spell must be at least one level below the highest spell level he can cast."
