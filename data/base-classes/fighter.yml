name: Fighter
sources:
  - Pathfinder Roleplaying Game Core Rulebook
tags: []
max_level: 20
hit_die: 10
starting_wealth: 5d6 x 10 gp (average 175 gp)
class_skills:
  - Climb
  - Craft
  - Handle Animal
  - Intimidate
  - Knowledge (dungeoneering)
  - Knowledge (engineering)
  - Profession
  - Ride
  - Survival
  - Swim
skill_ranks_per_level: 2
bab_progression: full
fort_progression: good
ref_progression: poor
will_progression: poor
weapon_proficiencies:
  - Simple
  - Martial
armor_proficiencies:
  - Light
  - Medium
  - Heavy
shield_proficiencies:
  - Shields
  - Tower Shields
class_features:
  - name: Bonus Feats
    levels:
      - 1
      - 2
      - 4
      - 6
      - 8
      - 10
      - 12
      - 14
      - 16
      - 18
      - 20
    text: |
      At 1st level, and at every even level thereafter, a fighter gains a bonus
      feat in addition to those gained from normal advancement (meaning that
      the fighter gains a feat at every level). These bonus feats must be
      selected from those listed as Combat Feats, sometimes also called
      “fighter bonus feats.”

      Upon reaching 4th level, and every four levels thereafter (8th, 12th, and
      so on), a fighter can choose to learn a new bonus feat in place of a
      bonus feat he has already learned. In effect, the fighter loses the bonus
      feat in exchange for the new one. The old feat cannot be one that was
      used as a prerequisite for another feat, prestige class, or other
      ability. A fighter can only change one feat at any given level and must
      choose whether or not to swap the feat at the time he gains a new bonus
      feat for the level.
    faq: |
      Can I use the Bonus Feats class feature to retrain a feat that I gained
      at 1st level (such as Cleave) to gain a feat that I did not qualify for
      at 1st level, but do qualify for now (such as Lunge)?

      Yes. So long as the feat that you “lose” is not used as a prerequisite
      for any other feat, prestige class, or other ability, you can gain any
      feat that you qualify for at the time that you retrain it.

      [Source]
  - name: Bravery
    type: Ex
    levels:
      - 2
      - 6
      - 10
      - 14
      - 18
    levels_text:
      2: "+1"
      6: "+2"
      10: "+3"
      14: "+4"
      18: "+5"
    text: |
      Starting at 2nd level, a fighter gains a +1 bonus on Will saves against
      fear. This bonus increases by +1 for every four levels beyond 2nd.
  - name: Armor Training
    type: Ex
    levels:
      - 3
      - 7
      - 11
      - 15
    text: |
      Starting at 3rd level, a fighter learns to be more maneuverable while
      wearing armor. Whenever he is wearing armor, he reduces the armor check
      penalty by 1 (to a minimum of 0) and increases the maximum Dexterity
      bonus allowed by his armor by 1. Every four levels thereafter (7th, 11th,
      and 15th), these bonuses increase by +1 each time, to a maximum –4
      reduction of the armor check penalty and a +4 increase of the maximum
      Dexterity bonus allowed.

      In addition, a fighter can also move at his normal speed while wearing
      medium armor. At 7th level, a fighter can move at his normal speed while
      wearing heavy armor.
  - name: Weapon Training
    type: Ex
    levels:
      - 5
      - 9
      - 13
      - 17
    text: |
      Starting at 5th level, a fighter can select one group of weapons, as
      noted below. Whenever he attacks with a weapon from this group, he gains
      a +1 bonus on attack and damage rolls.

      Every four levels thereafter (9th*, 13th, and 17th), a fighter becomes
      further trained in another group of weapons. He gains a +1 bonus on
      attack and damage rolls when using a weapon from this group. In addition,
      the bonuses granted by previous weapon groups increase by +1 each. For
      example, when a fighter reaches 9th level, he receives a +1 bonus on
      attack and damage rolls with one weapon group and a +2 bonus on attack
      and damage rolls with the weapon group selected at 5th level. Bonuses
      granted from overlapping groups do not stack. Take the highest bonus
      granted for a weapon if it resides in two or more groups.

      A fighter also adds this bonus to any combat maneuver checks made with
      weapons from his group. This bonus also applies to the fighter’s Combat
      Maneuver Defense when defending against disarm and sunder attempts made
      against weapons from this group.

      Weapon groups are defined as follows (GMs may add other weapons to these
      groups, or add entirely new groups):

      Axes: axe-gauntlet, dwarven heavy, axe-gauntlet, dwarven light, bardiche,
      battleaxe, boarding axe, butchering axe, collapsible kumade, dwarven
      waraxe, gandasa, greataxe, handaxe, heavy pick, hooked axe, knuckle axe,
      kumade, kumade, collapsible, light pick, mattock, orc double axe, pata,
      throwing axe, and tongi.

      Blades, Heavy: ankus, bastard sword, chakram, cutlass, double, double
      chicken saber, double walking stick katana, dueling sword, elven curve
      blade, estoc, falcata, falchion, flambard, greatsword, great terbutje,
      greatsword, katana, khopesh, klar, longsword, nine-ring broadsword,
      nodachi, rhoka sword, sawtooth sabre, scimitar, scythe, seven-branched
      sword, shotel, sickle-sword, split-blade sword, switchscythe, temple
      sword, terbutje, and two-bladed sword.

      Blades, Light: bayonet, broken-back seax, butterfly knife, butterfly
      sword, chakram, dagger, deer horn knife, dogslicer, drow razor, dueling
      dagger, gladius, hunga munga, kama, katar, kerambit, kukri, machete,
      madu, manople, pata, quadrens, rapier, sanpkhang, sawtooth sabre,
      scizore, shortsword, sica, sickle, spiral rapier, starknife, swordbreaker
      dagger, sword cane, wakizashi, and war razor.

      Bows: composite longbow, composite shortbow, longbow, orc hornbow, and
      shortbow.

      Close: axe-gauntlet, dwarven heavy, axe-gauntlet, dwarven light, bayonet,
      brass knuckles, cestus, dan bong, dwarven war-shield, emei piercer,
      fighting fan, gauntlet, heavy shield, iron brush, katar, klar, light
      shield, madu, mere club, punching dagger, rope gauntlet, sap, scizore,
      spiked armor, spiked gauntlet, spiked shield, tekko-kagi, tonfa,
      tri-bladed katar, unarmed strike, wooden stake, waveblade, and wushu dart.

      Crossbows: double crossbow, hand crossbow, heavy crossbow, launching
      crossbow, light repeating crossbow, heavy repeating crossbow, pelletbow,
      dwarven heavy, pelletbow, dwarven light, and tube arrow shooter,
      underwater heavy crossbow, underwater light crossbow

      Double: bo staff, boarding gaff, chain-hammer, chain spear, dire flail,
      double walking stick katana, double-chained kama, dwarven urgrosh, gnome
      battle ladder, gnome hooked hammer, kusarigama, monk’s spade, orc double
      axe, quarterstaff, taiaha, two-bladed sword, and weighted spear.

      Firearms: all one-handed, two-handed, and siege firearms.

      Flails: battle poi, bladed scarf, cat-o’-nine-tails, chain spear, dire
      flail, double chained kama, dwarven dorn-dergar, flail, flying talon,
      gnome pincher, halfling rope-shot, heavy flail, kusarigama, kyoketsu
      shoge, meteor hammer, morningstar, nine-section whip, nunchaku,
      sansetsukon, scorpion whip, spiked chain, urumi, and whip.

      Hammers: aklys, battle aspergillum, chain-hammer, club, earth breaker,
      gnome piston maul, greatclub, heavy mace, lantern staff, light hammer,
      light mace, mere club, planson, ram hammer, dwarven, sphinx hammer,
      dwarven, taiaha, tetsubo, wahaika, and warhammer.

      Monk: bo staff, brass knuckles, butterfly sword, cestus, dan bong, deer
      horn knife, double chained kama, double chicken saber, emei piercer,
      fighting fan, hanbo, jutte, kama, kusarigama, kyoketsu shoge, lungshuan
      tamo, monk’s spade, nine-ring broadsword, nine-section whip, nunchaku,
      quarterstaff, rope dart, sai, sanpkhang, sansetsukon, seven-branched
      sword, shang gou, shuriken, siangham, temple sword, tiger fork, tonfa,
      traveling kettle, tri-point double-edged sword, unarmed strike, urumi,
      and wushu dart.

      Natural: unarmed strike and all natural weapons, such as bite, claw,
      gore, tail, and wing.

      Polearms: bardiche, bec de corbin, bill, boarding gaff, crook, fauchard,
      giant-sticker, dwarven, glaive, glaive-guisarme, gnome ripsaw glaive,
      guisarme, halberd, hooked lance, horsechopper, lucerne hammer,
      mancatcher, monk’s spade, naginata, nodachi, ogre hook, ranseur,
      rhomphaia, tepoztopilli, and tiger fork.

      Siege engines: Ballista, bombard, catapult, corvus, firedrake, firewyrm,
      gallery, ram, siege tower, springal, trebuchet, and all other siege
      engines.

      Spears: amentum, boar spear, chain spear, double spear, elven branched
      spear, harpoon, javelin, lance, longspear, orc skull ram, pilum, planson,
      shortspear, sibat, spear, stormshaft javelin, tiger fork, trident, and
      weighted spear.

      Thrown: aklys, amentum, atlatl, blowgun, bolas, boomerang, brutal bolas,
      chain-hammer, chakram, club, dagger, dart, deer horn knife, dueling
      dagger, flask thrower, halfling sling staff, harpoon, hunga munga,
      javelin, kestros, lasso, light hammer, net, pilum, poisoned sand tube,
      ram hammer, dwarven, rope dart, shortspear, shuriken, sibat, sling, sling
      glove, snag net, spear, sphinx hammer, dwarven, starknife, stormshaft
      javelin, throwing axe, throwing shield, trident, and wushu dart.

      Tribal: Club, dagger, greatclub, handaxe, heavy shield, light shield,
      shortspear, spear, throwing axe, and unarmed strike.
  - name: Armor Mastery
    type: Ex
    levels:
      - 19
    text: |
      At 19th level, a fighter gains damage reduction 5/— whenever he is
      wearing armor or using a shield.
  - name: Weapon Mastery
    type: Ex
    levels:
      - 20
    text: |
      At 20th level, a fighter chooses one weapon, such as the longsword,
      greataxe, or longbow. Any attacks made with that weapon automatically
      confirm all critical threats and have their damage multiplier increased
      by 1 (×2 becomes ×3, for example). In addition, he cannot be disarmed
      while wielding a weapon of this type.
text: |
  Some take up arms for glory, wealth, or revenge. Others do battle to prove
  themselves, to protect others, or because they know nothing else. Still
  others learn the ways of weaponcraft to hone their bodies in battle and prove
  their mettle in the forge of war. Lords of the battlefield, fighters are a
  disparate lot, training with many weapons or just one, perfecting the uses of
  armor, learning the fighting techniques of exotic masters, and studying the
  art of combat, all to shape themselves into living weapons. Far more than
  mere thugs, these skilled warriors reveal the true deadliness of their
  weapons, turning hunks of metal into arms capable of taming kingdoms,
  slaughtering monsters, and rousing the hearts of armies. Soldiers, knights,
  hunters, and artists of war, fighters are unparalleled champions, and woe to
  those who dare stand against them.

  **Role:** Fighters excel at combat—defeating their enemies, controlling the
  flow of battle, and surviving such sorties themselves. While their specific
  weapons and methods grant them a wide variety of tactics, few can match
  fighters for sheer battle prowess.

  **Alignment:** Any.
