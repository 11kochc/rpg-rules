import os

from .reader import read_data
from .models import Character

def get_all_characters():
    """Read each file from the directory, and create an instance of the dataclass from it.

    This skips over files that have a name that start with '.'

    Args:
        dataclass ([type]): The dataclass to convert to.
        path (str, optional): The path of the directory to read. Defaults to None.

    Returns:
        list: A list of dataclasses, one for each file in the array.
    """
    results = list()
    for fname in sorted(filter(lambda name: name.endswith('.yml'), os.listdir("characters"))):
        if fname.startswith('.'):
            # Allows me to comment out files, without losing them.
            continue
        try:
            data = read_data("characters", fname[0:-4])
            data['id'] = fname[0:-4]

            results.append(Character(fname[0:-4], data))

        except TypeError:
            print(f'unable to convert {fname}')
            raise
    return results
