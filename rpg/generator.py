"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import shutil
from typing import Dict
import os

from jinja2 import Environment

from .models import BaseClass, Race, Skill, Spell, ClassChoices
from .reader import read_dir, get_cached_dataclasses

from .character_factory import get_all_characters

def copy_static():
    """Copies the necessary static files into the public directory."""
    try:
        for root, dirs, files in os.walk('public'):
            for file in files:
                os.remove(os.path.join(root, file))
            for path in dirs:
                shutil.rmtree(os.path.join(root, path))
    except FileNotFoundError:
        # if the dir doesn't exist, we don't need to delete it.
        pass
    shutil.copytree('static', os.path.join('public', 'static'))


def generate_site(jinja_environment: Environment, base: str):
    """Generate the pages necessary for the site.

    Args:
        jinja_environment (Environment): The jinja environment to use to do the generation
        base (str): The base to use for the generated pages.
    """
    copy_static()

    skills = read_dir(Skill)
    characters = get_all_characters()
    classes = read_dir(BaseClass)
    races = read_dir(Race)

    base_template_info = {
        'base': base,
        'characters': characters,
        'classes': classes,
        'races': races,
        'skills': skills,
    }
    generate_multiple_pages(jinja_environment, characters, base_template_info,
        'character/_layout.html.j2', 'characters')
    generate_multiple_pages(jinja_environment, classes, base_template_info,
        'classes/_class.html.j2', 'base-classes')
    generate_multiple_pages(jinja_environment, races, base_template_info,
        'races/_race.html.j2', 'races')
    generate_multiple_pages(jinja_environment, skills, base_template_info,
        'skills/_skill.html.j2', 'skills')
    generate_multiple_pages(jinja_environment, get_cached_dataclasses(Spell), base_template_info,
        'spells/_spell.html.j2', 'spells')

    generate_class_choices(jinja_environment, base_template_info)

    generate_index(jinja_environment, base_template_info)
    generate_license(jinja_environment, base_template_info)


def generate_multiple_pages(
    jinja_environment: Environment,
    items: list,
    base_template_info: Dict,
    template: str,
    path: str,
):
    template = jinja_environment.get_template(template)
    if path.startswith("data"):
        # Want seperate character and data in the repo, but not in the outputted site.
        # Add 1 for the os separator.
        path = path[len("data") + 1:]
    os.mkdir(os.path.join("public", path))
    for item in items:
        with open(os.path.join("public", path, f"{item.id}.html"), "wb" ) as file:
            html = template.render(page_model=item, **base_template_info)
            file.write(html.encode('utf-8'))


_OPTION_TYPES = [
    'alchemist-discoveries',
    'arcanist-exploits',
    'bloodlines',
    'domains',
    'ninja-tricks',
    'rage-powers',
    'rogue-talents',
    'slayer-talents',
    'warpriest-blessings',
    'wizard-schools',
]


def generate_class_choices(
        jinja_environment: Environment,
        base_template_info: Dict):
    """Generate the pages for the class options that have been provided.

    This method creates an ouptut directory for the class files, then loops over the generated
    characters, and renders the jinja template for each class that was provided.

    For a first pass, this method currently only supports blessings, but other options should be
    coming soon.

    Args:
        jinja_environment (Environment): The jinja environment to use to do the generation
        blessings (List[WarpriestBlessing]): The blessings that will be written to a page.
        base_template_info (Dict): The information needed for the base template.
    """
    template = jinja_environment.get_template('classes/_feature-choice.html.j2')

    for option_type in _OPTION_TYPES:
        options = read_dir(ClassChoices, os.path.join('data', f'{option_type}'))
        with open(os.path.join('public', f'{option_type}.html'), 'w+') as file:
            html = template.render(options=options, **base_template_info)
            file.write(html)


def generate_index(jinja_environment: Environment, base_template_info: Dict):
    """Generate the landing page for the site.

    Args:
        jinja_environment (Environment): The jinja environment to use to do the generation
        base_template_info (Dict): The information needed for the base template.
    """
    template = jinja_environment.get_template('_page.html.j2')
    index_text = "My site to organize the rules for my favorite RPG"
    index_title = "RPG Rules"
    html = template.render(**base_template_info, text=index_text, title=index_title)
    with open(os.path.join('public', 'index.html'), 'w+') as file:
        file.write(html)

def generate_license(jinja_environment: Environment, base_template_info: Dict):
    """Generate the landing page for the site.

    Args:
        jinja_environment (Environment): The jinja environment to use to do the generation
        base_template_info (Dict): The information needed for the base template.
    """
    template = jinja_environment.get_template('_page.html.j2')
    with open(os.path.join('data', 'LICENSE.md')) as file:
        html = template.render(**base_template_info, text=file.read(), title="Open Game License")
    with open(os.path.join('public', 'license.html'), 'w+') as file:
        file.write(html)
