"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from dataclasses import dataclass, field
import os
from typing import ClassVar, Dict, List
from .features import Feature
from ..helpers import capitalize_words, validate_mandatory_fields


@dataclass
class AgeInfo():
    adulthood: int
    intuitive: str
    self_taught: str
    trained: str
    middle_age: int
    old: int
    venerable: int
    maximum: str

@dataclass
class GenderBase():
    name: str
    base_height: str
    base_weight: str
    height_modifier: str

@dataclass
class Measurements():
    weight_modifier: str
    genders: List[GenderBase]

@dataclass
class Race():
    """Data class for races."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "races")
    id: str
    name: str
    sources: List[str]
    tags: List[str] = field(default_factory=list)
    summary: str = None
    ability_scores: str = None
    type: str = None
    subtypes: List[str] = field(default_factory=list)
    size: str = None
    ages: AgeInfo = None
    height_and_weight: Measurements = None
    text: str = None
    racial_traits: List[Feature] = field(default_factory=list)
    alternate_traits: Dict[str, str] = field(default_factory=dict)
    racial_subtypes: Dict[str, str] = field(default_factory=dict)
    favored_class_bonus: Dict[str, str] = field(default_factory=dict)

    def __post_init__(self):
        mandatory = [
            'ability_scores',
            'size',
            'text',
            'summary',
            'type',
            'height_and_weight',
            'ages',
            'racial_traits',
        ]
        validate_mandatory_fields(self, mandatory)
        self.name = capitalize_words(self.name)

        for feature in self.racial_traits:
            feature.link = f'base-classes/{self.id}.html#{feature.name}'
            feature.d20_link = f'https://www.d20pfsrd.com/races/core-races/{self.id}/'
            feature.aon_link = f'https://www.aonprd.com/RacesDisplay.aspx?ItemName={self.name}'
