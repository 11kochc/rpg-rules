"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from dataclasses import dataclass, field
from typing import ClassVar, Dict, List
import os

from .requirements import Requirements
from ..helpers import validate_mandatory_fields


@dataclass
class Item():
    DIRECTORY: ClassVar[str] = os.path.join("data", "items")
    id: str
    name: str
    sources: List[str]
    cost: float
    weight: float
    # Magic items will have these, but not all.
    aura: str = None
    caster_level: int = None
    slot: str = None
    construction_requirements: Requirements = field(default_factory=list)
    text: str = None

    def __post_init__(self):
        mandatory = [
            "text",
        ]
        validate_mandatory_fields(self, mandatory)

@dataclass
class Armor(Item):
    """A representation of the armor that a character can wear."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "armor")

    type: str = None
    armor_bonus: int = None
    max_dex: int = None
    armor_check_penalty: int = None
    arcane_spell_failure: int = None

    def __post_init__(self):
        mandatory = [
            "type",
            "armor_bonus",
            "armor_check_penalty",
        ]
        validate_mandatory_fields(self, mandatory)

@dataclass
class Weapon(Item):
    DIRECTORY: ClassVar[str] = os.path.join("data", "weapons")
    type: str = None
    damage: Dict[str, str] = None
    damage_type: str = None
    critical: str = None
    special: List[str] = field(default_factory=list)
    range_increment: int = None

    def __post_init__(self):
        mandatory = [
            "type",
            "damage",
            "damage_type",
            "critical",
        ]
        validate_mandatory_fields(self, mandatory)

@dataclass
class Inventory():
    weapons: List[Weapon]
    armor: List[Armor]
    items: List[Item]
    gold: int

    @property
    def weight(self):
        weight = 0
        for weapon in self.weapons:
            weight += weapon.weight

        for armor in self.armor:
            weight += armor.weight

        for item in self.items:
            weight += item.weight

        return weight

    @property
    def value(self):
        value = 0
        for weapon in self.weapons:
            value += weapon.cost

        for armor in self.armor:
            value += armor.cost

        for item in self.items:
            value += item.cost
        return value + self.gold
