from dataclasses import dataclass, field
from typing import List

@dataclass
class Requirements:
    feats: List[str] = field(default_factory=list)
    spells: List[str] = field(default_factory=list)
    special: List[str] = field(default_factory=list)
    cost: int = 0
