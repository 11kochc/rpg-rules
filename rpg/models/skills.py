"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from dataclasses import dataclass, field
import os
from typing import ClassVar, List
from ..helpers import validate_mandatory_fields

@dataclass
class Skill():
    """Data class for skills."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "skills")
    id: str
    name: str
    sources: List[str]
    ability_score: str
    armor_check_penalty: bool = False
    trained_only: bool = False
    text: str = None
    tags: List[str] = field(default_factory=list)

    def __post_init__(self):
        validate_mandatory_fields(self, ["text"])
