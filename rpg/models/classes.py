"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
from typing import ClassVar, List

from dataclasses import dataclass, field
from .features import AlternateFeature, Feature
from .spells import SpellsPerDay
from ..reader import read_dataclass
from ..helpers import validate_mandatory_fields


BAB_FACTORS = {
    'full': 1,
    'mid': .75,
    'slow': .5,
}


class LevelOverview():
    level: int
    bab: int
    fort_save: int
    ref_save: int
    will_save: int
    features: str
    spell_per_day: List[int]

@dataclass()
class ClassChoices():
    """Data class for the choices available to different classes.

    This serves as the base for many different choices available to different classes.
    It covers abilities that are available as soon as they are chosen, like rouge
    talents and rage powers, as well as decisions that unlock features as you level, like
    domains, and bloodlines.

    Each differnt choice type should be represented as a subclass, so that the DIRECTORY property
    can be set correctly.
    """
    name: str
    id: str
    sources: List[str]
    text: str = None
    features: List[Feature] = field(default_factory=list)

    def get_features_for_level(self, level: int):
        """Get a list of the features available to this class, at the given level.

        Args:
            level (int): The level to get features for.

        Returns:
            list: The features available at the provided level.
        """
        features = []
        if self.text:
            # if the choice itself has text, that should be included on anyone that has it selected.
            features.append(Feature(name=self.name, levels=[1], text=self.text))

        for feature in self.features:
            if level >= feature.levels[0]:
                features.append(feature)

        return features

@dataclass
class ClassInfo():
    """Contains the info for a class."""
    id: str
    name: str
    sources: List[str]
    tags: List[str]
    max_level: int
    hit_die: int
    prerequisites: List[str] = field(default_factory=list)
    starting_wealth: str = None
    class_skills: List[str] = field(default_factory=list)
    skill_ranks_per_level: int = None
    bab_progression: str = None
    fort_progression: str = None
    ref_progression: str = None
    will_progression: str = None
    spells_per_day: str = None
    spell_list: str = None
    spell_list_levels: int = 0
    spells_known: str = None
    weapon_proficiencies: List[str] = field(default_factory=list)
    armor_proficiencies: List[str] = field(default_factory=list)
    shield_proficiencies: List[str] = field(default_factory=list)
    class_features: List[Feature] = field(default_factory=list)
    text: str = None

    def __post_init__(self):
        mandatory = [
            'starting_wealth',
            'class_skills',
            'skill_ranks_per_level',
            'bab_progression',
            'fort_progression',
            'ref_progression',
            'text',
            'class_features',
        ]
        validate_mandatory_fields(self, mandatory)


    def get_bab_at_level(self, level):
        """Calculates the base attack bonus for

        Args:
            level (int): The level to calculate the BAB for.
        """
        return BAB_FACTORS[self.bab_progression] * level

    def get_save_at_level(self, save: str, level):
        if getattr(self, f"{save}_progression") == "good":
            return level * .5 + 2, True
        else:
            return level/3, False


    def get_spells_per_day_at_level(self, level):
        # This works for atleast Warpriests, hunters, bards, and magus.
        # If it doesn't work as we add more classes, we add more classes,
        # we may need to add it to the class files.


        if self.spells_per_day:
            spells_per_day = read_dataclass(SpellsPerDay, str(self.spells_per_day))
            return spells_per_day.spells_for_level(level)

        return dict()



    def get_table_row_for_level(self, level: int):
        """Generates the data for a row of the classes overview table.

        Args:
            level (int): The level being requested

        Returns:
            LevelOverview: An overview of the features gained at this level.
        """
        row = LevelOverview()
        row.level = level
        row.bab = int(self.get_bab_at_level(level))
        row.fort_save = int(self.get_save_at_level("fort", level)[0])
        row.will_save = int(self.get_save_at_level("will", level)[0])
        row.ref_save = int(self.get_save_at_level("ref", level)[0])

        feat_list = list()
        for feat in self.class_features:
            if feat.name == "Bonus Languages":
                continue
            feat_text = ""
            if level in feat.levels:
                feat_text = f"[{feat.name}]({feat.link})"
                if level in feat.levels_text:
                    feat_text += f" {feat.levels_text[level]}"
                feat_list.append(feat_text)

        row.features = ", ".join(feat_list)
        if not row.features:
            row.features += '-'

        row.spell_per_day = self.get_spells_per_day_at_level(level)

        return row

    def get_table_rows(self):
        for level in range(self.max_level):
            yield self.get_table_row_for_level(level + 1)



@dataclass()
class BaseClass(ClassInfo):
    """Data class for the RPG classes. These base classes do not contain any archetype information.
    """
    DIRECTORY: ClassVar[str] = os.path.join("data", "base-classes")

    def __post_init__(self):
        mandatory = [
            'starting_wealth',
            'class_skills',
            'skill_ranks_per_level',
            'bab_progression',
            'fort_progression',
            'ref_progression',
            'will_progression',
            'text',
            'class_features',
            'sources',
        ]
        validate_mandatory_fields(self, mandatory)


    # pylint: disable=function-redefined
    # This is defined above, so that it is picked up as a field for the dataclass.
    # But, we define the setter, and getter here.
    @property
    def class_features(self) -> List[Feature]:
        return self._class_feature

    @class_features.setter
    def class_features(self, features: List[Feature]):
        for feature in features:
            d20_name = feature.name.replace(' ', '_')
            if feature.type:
                d20_name += '_' + feature.type
            feature.link = f'base-classes/{self.id}.html#{feature.name}'
            feature.d20_link = f'https://www.d20pfsrd.com/classes/hybrid-classes/{self.id}/#{d20_name}'
            feature.aon_link = f'https://www.aonprd.com/ClassDisplay.aspx?ItemName={self.name}'
        self._class_feature = features


@dataclass
class AlternateSkills:
    """The skills that are added, and/or removed from the base class by the archetype."""
    add: List[str] = field(default_factory=list)
    remove: List[str] = field(default_factory=list)


@dataclass()
class Archetype():
    """The data class for archetypes."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "archetypes")
    id: str
    archetype_name: str
    base: str
    class_skills: AlternateSkills = field(default_factory=AlternateSkills)
    class_features: List[AlternateFeature] = None

    def __post_init__(self):
        mandatory = [
            'class_features',
        ]
        validate_mandatory_fields(self, mandatory)

class ArchetypeWrapper(ClassInfo):
    """A class that combines an archetype with it's base class to give the necessary information."""

    archetype: Archetype
    base_class: ClassInfo

    def __init__(self, base_class: ClassInfo, archetype: Archetype):
        self.base_class = base_class
        self.archetype = archetype

    @property
    def name(self) -> str:
        return f"{self.base_class.name} ({self.archetype.archetype_name})"

    @property
    def sources(self) -> List[str]:
        return set(self.base_class.sources + self.archetype.sources)

    @property
    def tags(self) -> List[str]:
        return set(self.base_class.tags + self.archetype.tags)

    @property
    def max_level(self) -> int:
        return self.base_class.max_level

    @property
    def hit_die(self) -> int:
        return self.base_class.hit_die

    @property
    def starting_wealth(self) -> str:
        return self.base_class.starting_wealth

    @property
    def class_skills(self) -> List[str]:
        class_skills = set(self.archetype.class_skills.add)
        for skill in self.base_class.class_skills:
            if skill not in self.archetype.class_skills.remove:
                class_skills.add(skill)

        return class_skills

    @property
    def skill_ranks_per_level(self) -> int:
        return self.base_class.skill_ranks_per_level

    @property
    def bab_progression(self) -> str:
        return self.base_class.bab_progression

    @property
    def fort_progression(self) -> str:
        return self.base_class.fort_progression

    @property
    def ref_progression(self) -> str:
        return self.base_class.ref_progression

    @property
    def will_progression(self) -> str:
        return self.base_class.will_progression

    @property
    def class_features(self) -> List[Feature]:
        replaced = dict()
        altered = dict()

        base_class = self.base_class
        while hasattr(base_class, 'base_class'):
            # There may be more than one level, if there is more than one archetyp.
            base_class = base_class.base_class


        d20_archetype_name = self.archetype.archetype_name.lower().replace(" ", "-")

        # Yay consistency!
        if d20_archetype_name != 'strangler':
            d20_archetype_name = f'{self.archetype.archetype_name.lower().replace(" ", "-")}-{base_class.id}-archetype'

        for feature in self.archetype.class_features:

            d20_feature_name = feature.name.replace(' ', '_')
            if feature.type:
                d20_feature_name += '_' + feature.type

            # Don't have archetype pages yet.
            # feature.link = f'classes/{self.id}.html#{feature.name}'
            feature.d20_link = f'https://www.d20pfsrd.com/classes/hybrid-classes/{base_class.id}/archetypes/paizo-{base_class.id}-archetypes/{d20_archetype_name}/#{d20_feature_name}'
            feature.aon_link = f'https://www.aonprd.com/ArchetypeDisplay.aspx?FixedName={base_class.name} {self.archetype.archetype_name}'

            for replaced_feature in feature.replaces:
                replaced[replaced_feature] = feature
            for altered_feature in feature.alters:
                altered[altered_feature] = feature

        features = dict()
        for feature in self.base_class.class_features:
            if feature.name in replaced:
                features[replaced[feature.name]] = None
            else:
                features[feature] = None
                if feature.name in altered:
                    altered_text = "This feature has been altered by the " + \
                        altered[feature.name].name + " feature of the " + \
                        self.archetype.archetype_name + " archetype."
                    feature.text += altered_text
                    features[altered[feature.name]] = None

        return list(features)

    @property
    def text(self) -> str:
        return self.base_class.text

    @property
    def id(self) -> str:
        return self.base_class.id

    @property
    def spell_list(self) -> str :
        return self.base_class.spell_list

    @property
    def spell_list_levels(self) -> int :
        return self.base_class.spell_list_levels

    @property
    def weapon_proficiencies(self) -> List[str] :
        return self.base_class.weapon_proficiencies

    @property
    def armor_proficiencies(self) -> List[str] :
        return self.base_class.armor_proficiencies

    @property
    def shield_proficiencies(self) -> List[str] :
        return self.base_class.shield_proficiencies

    @property
    def prerequisites(self) -> List[str] :
        return self.base_class.prerequisites
