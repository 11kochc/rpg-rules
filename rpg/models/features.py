"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from dataclasses import dataclass, field
from typing import Dict, List
from ..helpers import validate_mandatory_fields

@dataclass
class Feature():
    """Data class for the class features available to a class."""
    name: str
    type: str = ""
    levels: List[int] = field(default_factory=list)
    levels_text: Dict[int, str] = field(default_factory=list)
    text: str = None
    faq: str = None
    link: str = ""
    hidden: bool = False
    options: str = None
    aon_link: str = None
    d20_link: str = None

    def __post_init__(self):
        mandatory = [
            "text"
        ]
        validate_mandatory_fields(self, mandatory)

    def __hash__(self):
        return hash(self.name + self.link)

@dataclass
class AlternateFeature(Feature):
    """Data class for features that replace others, such as Archetypes."""
    alters: List[str] = field(default_factory=list)
    replaces: List[str] = field(default_factory=list)

    def __hash__(self):
        return hash(self.name + self.link)
