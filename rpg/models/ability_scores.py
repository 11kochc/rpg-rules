"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from dataclasses import dataclass

import math

def _get_mod_from_score(score):
    return math.floor((score - 10) / 2)


@dataclass
class AbilityScores:
    """The ability scores for a Character."""
    str: int
    dex: int
    con: int
    int: int
    wis: int
    cha: int


    def get_mod(self, ability: str):
        """Get the modifier for a given ability score.

        Args:
            ability (str): The abreviation for requested ability score. Can be
                * str
                * dex
                * con
                * int
                * wis
                * cha

        Raises:
            Exception: If ability is not a recognized ability score.

        Returns:
            int: The modifier for the requested ability score.
        """
        if ability.lower() == 'str':
            return self.get_str_mod()
        if ability.lower() == 'dex':
            return self.get_dex_mod()
        if ability.lower() == 'con':
            return self.get_con_mod()
        if ability.lower() == 'int':
            return self.get_int_mod()
        if ability.lower() == 'wis':
            return self.get_wis_mod()
        if ability.lower() == 'cha':
            return self.get_cha_mod()
        raise Exception(f'Invalid ability requested {ability}.')

    def get_str_mod(self):
        """Get the strength modifier for the character.

        Returns:
            int: The strength modifer for the character.
        """
        return _get_mod_from_score(self.str)

    def get_dex_mod(self):
        """Get the dexterity modifier for the character.

        Returns:
            int: The dexterity modifer for the character.
        """
        return _get_mod_from_score(self.dex)

    def get_con_mod(self):
        """Get the constitution modifier for the character.

        Returns:
            int: The constitution modifer for the character.
        """
        return _get_mod_from_score(self.con)

    def get_int_mod(self):
        """Get the intelligence modifier for the character.

        Returns:
            int: The intelligence modifer for the character.
        """
        return _get_mod_from_score(self.int)

    def get_wis_mod(self):
        """Get the wisdom modifier for the character.

        Returns:
            int: The wisdom modifer for the character.
        """
        return _get_mod_from_score(self.wis)

    def get_cha_mod(self):
        """Get the charisma modifier for the character.

        Returns:
            int: The charisma modifer for the character.
        """
        return _get_mod_from_score(self.cha)
