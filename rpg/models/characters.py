"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from typing import ClassVar, Dict, List, Set, Iterable
import os

from dataclasses import dataclass, field

from ..reader import read_dataclass
from .ability_scores import AbilityScores
from .calculation import Calculation
from .classes import Archetype, ArchetypeWrapper, BaseClass, ClassChoices
from .equipment import Armor, Inventory, Item, Weapon
from .features import Feature
from .races import Race
from .skills import Skill
from .spells import Spell

_DEFAULT_TO_HIT = None


@dataclass
class Attack():
    """This class represents an attack that a character can take"""
    name: str
    damage: Calculation
    damage_type: str
    to_hit: Calculation
    range: int = None
    critical: str = "x2"

    @classmethod
    def get_attack_from_weapon(cls, weapon: Weapon, overrides: dict, character_values: dict):
        name = overrides.get('name', None) or weapon.name

        damage_default = weapon.damage
        damage_default.setdefault('ability', 'str')

        damage = Calculation.create(overrides.get('damage', dict()), damage_default, character_values, 'weapon')

        to_hit_default = {
            'bab': 'bab',
            'ability': 'str'
        }

        to_hit = Calculation.create(overrides.get('to_hit', dict()), to_hit_default, character_values, 'ability')

        return cls(name=name, damage=damage, damage_type=weapon.damage_type, range=weapon.range_increment, to_hit=to_hit, critical=weapon.critical)

@dataclass
class NamedCalculation():
    name: str
    calculation: Calculation

@dataclass
class CharacterDescription():
    """The various "fluff" stats that help describe a character."""
    name: str
    # Making age required, just to not have to special case the int.
    age: int = None
    player: str = ''
    height: str = ''
    weight: str = ''
    alignment: str = ''
    diety: str = ''
    region: str = ''
    gender: str = ''



@dataclass
class ClassLevels():
    """Represents the levels a character has in a single class."""
    class_: BaseClass
    level: int

    def get_bab(self):
        """Get's the bab contribued by this class.

        Returns:
            int: The bab for this class and level.
        """
        return self.class_.get_bab_at_level(self.level)


@dataclass
class SkillScores():
    """A representation of the various modifiers that can affect a skill."""
    ranks: int
    modifiers: Dict[str, int] = field(default_factory=dict)


@dataclass
class Feat():
    """Feats that can be taken by a character."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "feats")

    id: str
    name: str
    sources: List[str]
    flavor_text: str
    benefit: str
    text: str = ""
    normal: str = None
    special: str = None
    prerequisites: List[str] = field(default_factory=list)
    tags: List[str] = field(default_factory=list)

    # Don't have feat pages yet.
    @property
    def link(self):
        return f"feats/{self.name}.html"

    @property
    def d20_link(self):
        if self.tags:
            return f"https://www.d20pfsrd.com/feats/combat-feats/{self.id}-{'-'.join(self.tags)}"
        else:
            return f"https://www.d20pfsrd.com/feats/general-feats/{self.id}"

    @property
    def aon_link(self):
        return f"https://www.aonprd.com/FeatDisplay.aspx?ItemName={self.name}"


class Character():
    """An RPG Character.

    Keeps the initial data it was given, and uses that to calculate required fields as needed.
    """

    def __init__(self, identifier, data: dict):
        self._armor = None
        self._shield = None
        self._class_levels = None
        self._feats = None
        self.data = data
        self.id = identifier
        self._attacks = dict()

    @property
    def description(self) -> CharacterDescription:
        return CharacterDescription(**self.data.get('description', dict()))

    @property
    def speed(self) -> int:
        return self.data['speed']

    @property
    def total_hp(self) -> int:
        defaults = dict()

        first = True
        total_levels = 0
        for class_and_level in self.class_levels:
            levels = class_and_level.level
            total_levels += levels
            hit_die = class_and_level.class_.hit_die
            hp = 0
            if first:
                # First hit die is full
                # so needs to be treated differently.
                levels = levels - 1
                first = False
                hp = hit_die
            hp = hp + int(((levels * (hit_die + 1)) + 1)/2)

            defaults[class_and_level.class_.name] = hp

        defaults["con"] = self.ability_scores.get_con_mod() * total_levels


        return Calculation.create(self.data.get('hp', dict()), defaults, self.calc_values, 'base')

    @property
    def alignment(self) -> int:
        return self.data['description']['alignment']

    @property
    def diety(self) -> int:
        return self.data['description'].get('diety', None)

    @property
    def bab(self) -> int:
        """Returns the characters Base attack bonus.

        Returns:
            int: The characters base attack bonus.
        """
        return int(sum(map(lambda cl: cl.get_bab(), self.class_levels)))


    @property
    def shield(self) -> Armor:
        # Cache the result on _shield
        if self._shield:
            return self._shield
        if 'shield' not in self.data or not self.data['shield']:
            return None

        shield = read_dataclass(Armor, self.data['shield'])
        self._shield = shield

        return self._shield

    @property
    def armor(self) -> Armor:
        # Cache the result on _armor
        if self._armor:
            return self._armor
        if 'armor' not in self.data or not self.data['armor']:
            return None

        armor = read_dataclass(Armor, self.data['armor'])
        self._armor = armor

        return self._armor

    @property
    def class_levels(self) -> List[ClassLevels]:
        if self._class_levels:
            return self._class_levels

        self._class_levels = list()
        for item in self.data['class_levels']:
            class_info = read_dataclass(BaseClass, item['class_'])

            for archetype_name in item.get('archetypes', list()):
                archetype = read_dataclass(Archetype, f"{item['class_']}_{archetype_name}")
                class_info = ArchetypeWrapper(class_info, archetype)

            self._class_levels.append(ClassLevels(class_info, item['level']))

        return self._class_levels

    @property
    def race(self) -> Race:
        return read_dataclass(Race, self.data['race'])

    @property
    def name(self) -> str:
        return self.description.name

    @property
    def ability_scores(self) -> AbilityScores:
        scores = self.data['ability_scores']
        return AbilityScores(**scores)

    def get_skill_ranks(self, skill_name):
        """Get's the characters ranks in a specified skill.

        Args:
            skill_name (str): The name of the skill that we are getting the ranks for.

        Returns:
            int: The number of ranks the character has.
        """
        return self._get_skill_modifiers(skill_name).get('ranks', 0)

    def _get_class_skills(self) -> Set[str]:
        skills = set()
        for class_level in self.class_levels:
            skills |= set(class_level.class_.class_skills)
        return skills

    def _get_skill_modifiers(self, skill_name):
        mods = dict()
        if skill_name in self.data['skill_ranks']:
            if isinstance(self.data['skill_ranks'][skill_name], int):
                mods = { "ranks": self.data['skill_ranks'][skill_name] }
            else:
                mods = self.data['skill_ranks'][skill_name]

        return mods


    def get_skill_calculation(self, skill: Skill):
        class_skills = self._get_class_skills()
        modifiers = self._get_skill_modifiers(skill.name)

        defaults = {}

        if self.armor and self.armor.armor_check_penalty != 0 and skill.armor_check_penalty:
            defaults['armor'] = self.armor.armor_check_penalty
        if self.shield and self.shield.armor_check_penalty != 0 and skill.armor_check_penalty:
            defaults['shield'] = self.shield.armor_check_penalty

        if skill.name in class_skills and modifiers.get('ranks', 0) > 0:
            defaults['class'] = 3

        defaults['ability'] = skill.ability_score

        calculation = Calculation.create(modifiers, defaults, self.calc_values, 'ranks')
        return calculation

    @property
    def attacks(self):
        """Get's the attacks the character can make.

        Returns:
            list: The attacks the character can make.
        """
        if self._attacks:
            return self._attacks
        attacks = list()
        for attack in self.data.get('attacks', list()):
            if 'weapon' in attack:
                weapon = read_dataclass(Weapon, attack['weapon'])
                attacks.append(Attack.get_attack_from_weapon(weapon, attack, self.calc_values))
            else:
                default_damage = {'ability': 'str'}
                default_to_hit = {
                    'ability': 'str',
                    'bab': 'bab',
                }
                attack['damage'] = \
                    Calculation.create(attack.pop('damage', dict()), default_damage, self.calc_values)
                attack['to_hit'] = \
                    Calculation.create(attack.pop('to_hit', dict()), default_to_hit, self.calc_values)

                # type is a valid entry, but not relevant for now. So just remove from dict.
                attack.pop('type', None)
                attacks.append(Attack(**attack))

        self._attacks = attacks
        return attacks

    @property
    def feats(self) -> List[Feat]:
        if self._feats:
            return self._feats

        self._feats = list()
        for feat in self.data['feats']:
            self._feats.append(read_dataclass(Feat, feat))
        return self._feats

    @property
    def features(self):
        """Gets a list of the features that the character has.

        Returns:
            list: the features that the character has.
        """
        features = []
        for class_ in self.class_levels:
            for feature in class_.class_.class_features:
                if feature.levels and class_.level >= feature.levels[0]:
                    features.append(feature)

                if feature.options:
                    for choice in self.chosen_class_options[feature.options]:
                        features += choice.get_features_for_level(class_.level)

        for feat in self.feats:
            features.append(Feature(name=feat.name, text=feat.benefit, link=feat.link, aon_link=feat.aon_link, d20_link=feat.d20_link))

        for feature in self.race.racial_traits:
            features.append(feature)

        return features

    def get_save(self, ability, save):
        modifiers = self.data.get(f'{save}_save', dict())
        defaults = dict()
        defaults['ability'] = ability

        good_bonus = 0
        other_bonus = 0
        for levels in self.class_levels:
            base_save, is_good = levels.class_.get_save_at_level(save, levels.level)
            if is_good:
                good_bonus = base_save if good_bonus == 0 else good_bonus + base_save - 2
            else:
                other_bonus += base_save

        defaults['base'] = int(good_bonus + other_bonus)
        calculation = Calculation.create(modifiers, defaults, self.calc_values, 'base')
        return calculation


    @property
    def fort_save(self) -> Calculation:
        return self.get_save('con', 'fort')

    @property
    def ref_save(self) -> Calculation:
        return self.get_save('dex', 'ref')

    @property
    def will_save(self) -> Calculation:
        return self.get_save('wis', 'will')

    def get_ac_bonuses(self, touch=False, flat=False):
        """Get the AC bonuses for the character.

        Returns:
            dict: The AC bonuses for the character.
        """
        touch_excludes = ['armor', 'shield', 'natural']
        flat_excludes = ['dex', 'dodge']
        bonuses = self.data.get('ac_bonus', dict()).copy()

        bonuses['base'] = 10
        if self.armor:
            bonuses['armor'] = bonuses.get('armor', self.armor.armor_bonus)
        if self.shield:
            bonuses['shield'] = bonuses.get('shield', self.shield.armor_bonus)

        if not 'dex' in bonuses:
            bonuses['dex'] = self.ability_scores.get_dex_mod()

        if touch:
            for key in touch_excludes:
                bonuses.pop(key, None)

        if flat:
            for key in flat_excludes:
                bonuses.pop(key, None)

        return bonuses

    def get_ac(self, touch=False, flat=False):
        """Get the AC for the character.

        Returns:
            int: The AC for the character.
        """
        return Calculation.create(self.get_ac_bonuses(touch=touch, flat=flat), dict(), self.calc_values, 'base')


    @property
    def cmd(self) -> List[NamedCalculation]:
        """Get the CMD for the character.

        Returns:
            int: The CMD for the character.
        """
        maneuvers = list()

        defaults = self.get_ac_bonuses(touch=True)
        defaults['str'] = 'str'
        defaults['bab'] = 'bab'

        main = Calculation.create(self.data.get('cmd', dict()).get('bonuses', dict()), defaults, self.calc_values, 'base')
        for maneuver, bonuses in self.data.get('cmd', dict()).items():
            if maneuver == 'bonuses':
                continue
            maneuvers.append(
                NamedCalculation(
                    maneuver,
                    Calculation.create(bonuses, main.modifiers, self.calc_values, 'base')
                )
            )


        maneuvers.append(NamedCalculation("", main))

        return maneuvers


    @property
    def initiative(self) -> int:
        """Get the initiative for the character.

        Returns:
            int: The initiative for the character.
        """
        return Calculation.create(self.data.get('initiative', dict()), { 'ability': 'dex' }, self.calc_values, 'ability')

    @property
    def cmb(self) -> List[NamedCalculation]:
        """Get the CMB for the character.

        Returns:
            int: The CMB for the character.
        """
        defaults = { 'bab': 'bab', 'ability': 'str' }

        maneuvers = list()

        main = Calculation.create(self.data.get('cmb', dict()).get('bonuses', dict()), defaults, self.calc_values, 'bab')
        for maneuver, bonuses in self.data.get('cmb', dict()).items():
            if maneuver == 'bonuses':
                continue
            maneuvers.append(
                NamedCalculation(
                    maneuver,
                    Calculation.create(bonuses, main.modifiers, self.calc_values, 'bab')
                )
            )

        maneuvers.append(NamedCalculation("", main))
        return maneuvers


    @property
    def chosen_class_options(self) -> Dict[str, ClassChoices]:
        chosen_option_dict = dict()

        for option_type, choices in self.data.get('chosen_class_options', dict()).items():
            chosen_option_dict[option_type] = list()
            for choice in choices:
                chosen_option_dict[option_type].append(read_dataclass(ClassChoices, choice, os.path.join("data", option_type.replace('_', '-'))))

        return chosen_option_dict

    @property
    def calc_values(self) -> Dict[str, int]:
        calc_values = dict()
        for ability in ('str', 'dex', 'con', 'int', 'wis', 'cha'):
            calc_values[ability] = self.ability_scores.get_mod(ability)
        calc_values['bab'] = self.bab
        return calc_values

    @property
    def spells(self) -> Iterable:
        if 'spells' in self.data:
            for spell in self.data['spells']:
                if isinstance(spell, str):
                    spell_id = spell.replace(' ', '-').lower()
                    yield read_dataclass(Spell, spell_id)

    @property
    def inventory(self) -> Iterable[Item]:
        if not 'inventory' in self.data:
            return None

        weapons = list()
        armors = list()
        items = list()
        gold = 0
        if 'weapons' in self.data['inventory']:
            for weapon in self.data['inventory']['weapons']:
                weapons.append(read_dataclass(Weapon, weapon))

        if self.armor:
            armors.append(self.armor)
        if self.shield:
            armors.append(self.shield)
        if 'armor' in self.data['inventory']:
            for armor in self.data['inventory']['armor']:
                armors.append(read_dataclass(Armor, armor))

        if 'items' in self.data['inventory']:
            for item in self.data['inventory']['items']:
                items.append(read_dataclass(Item, item))

        if 'gold' in self.data['inventory']:
            gold = self.data['inventory']['gold']

        return Inventory(weapons, armors, items, gold)
