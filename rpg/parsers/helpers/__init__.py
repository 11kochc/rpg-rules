from rpg.models import Item, Requirements

from collections import namedtuple
import os
import re
import urllib.request

from bs4 import BeautifulSoup, NavigableString


URL_REGEX = re.compile(r"(https?)://([a-z.0-9]*)/([^#]*)(.*)")

Url = namedtuple('Url', ['scheme', 'host', 'path', 'fragment'])

def parse_url(url) -> Url:
    matches = URL_REGEX.match(url)
    scheme = matches.group(1)
    host = matches.group(2)
    path = matches.group(3)
    fragment = matches.group(4)
    return Url(scheme, host, path, fragment)

def get_soup(url) -> str:
    # Used a cache during dev. may as well keep, incase I want to run this a couple times
    parsed_url = parse_url(url)
    path_parts = list(os.path.split(parsed_url.path))
    if parsed_url.path.endswith('/'):
        path_parts.append('index.html')
    cache_path = os.path.join(".cache", parsed_url.host, *path_parts)

    try:
        with open(cache_path, 'r') as file:
            return BeautifulSoup(file, features="html.parser")
    except FileNotFoundError:
        try:
            os.makedirs(os.path.dirname(cache_path))
        except FileExistsError:
            pass
        with urllib.request.urlopen(url) as response:
            with open(cache_path, 'w') as file:
                for line in response:
                    file.write(line.decode('utf-8'))

    with open(cache_path, 'r') as file:
        return BeautifulSoup(file, features="html.parser")

def has_class(element, class_name):
    return 'class' in element.attrs and class_name in element.attrs['class']

def get_direct_text(container_element):
    inner_text = [element for element in container_element if isinstance(element, NavigableString)]
    return "\n".join(inner_text)

def get_next_sibling_by_name(element, name):
    while element.next_sibling.name != name:
        element = element.next_sibling
    return element.next_sibling
