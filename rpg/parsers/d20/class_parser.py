import re
from rpg.models import BaseClass, Feature
from ..helpers import has_class, get_direct_text, get_next_sibling_by_name

STARTING_WEALTH_REGEX = re.compile(
    r"(\d*d\d{1,2} ?[×x] ?\d* ?gp ?\( ?(?:average|avg\.?) \d+ ?gp.? ?\))" )

bab_map = {
    1: "slow",
    2: "mid",
    3: "full",
}

save_map = {
    1: "poor",
    3: "good",
}

def parse_class(soup, identifier, sources):
    article = soup.article
    name = article.h1.text

    print(f"Name: {name}")

    table_rows = soup.table.tbody.findChildren("tr")

    third_level = table_rows[2]

    bab = bab_map[int(third_level.findChildren("td")[1].text)]
    fort = save_map[int(third_level.findChildren("td")[2].text)]
    ref = save_map[int(third_level.findChildren("td")[3].text)]
    will = save_map[int(third_level.findChildren("td")[4].text)]





    max_level = len(table_rows)
    text = ""
    prerequisites = list()
    hit_die = None
    starting_wealth = None
    class_skills = list()
    skill_ranks = None
    class_features = None

    for element in soup.find("div", { "class": "article-content"}).findChildren():
        if element.name == "p" and not has_class(element, 'toc_title'):
            # Want to see if I cna just do this here, rather than in each case.
            direct_text = get_direct_text(element).strip(':').strip()
            if not element.b:
                if not text:
                    text = element.text
            elif "role" in element.b.text.lower():
                text += f"\n\n**Role:** {direct_text}"
            elif "alignment" in element.b.text.lower():
                alignment_text = get_direct_text(element).strip(':').strip()
                text += f"\n\n**Alignment:** {alignment_text}"
                prerequisites.append(alignment_text)
            elif "hit die" in element.b.text.lower():
                hit_die = int(direct_text.strip('d').strip('.'))
            elif "starting wealth" in element.b.text.lower():
                match = STARTING_WEALTH_REGEX.match(direct_text)
                starting_wealth = match.group(0)
            elif "skill ranks per level" in element.b.text.lower():
                skill_ranks = int(re.match(r"\d+", direct_text).group(0))

            if 'class skills' in element.text and element.a:
                for a in element.findChildren("a"):
                    if 'href' in a.attrs and 'skills' in a.attrs['href']:
                        if "skills" not in a.text.lower():
                            class_skills.append(a.text)
        elif element.name == "h3" and "class features" in element.text.lower():
            h4 = get_next_sibling_by_name(element, 'h4')

            class_features = list(parse_features(h4, table_rows))



    print(f"Text: {text}")
    print(f"skill_ranks: {skill_ranks}")
    print(f"bab: {bab}")
    print(f"fort: {fort}")
    print(f"ref: {ref}")
    print(f"will: {will}")

    return BaseClass(
        id=identifier,
        name=name,
        sources=sources,
        tags=[],
        max_level=max_level,
        hit_die=hit_die,
        prerequisites=prerequisites,
        starting_wealth=starting_wealth,
        class_skills=class_skills,
        skill_ranks_per_level=skill_ranks,
        bab_progression=bab,
        fort_progression=fort,
        ref_progression=ref,
        will_progression=will,
        class_features=class_features,
        text=text
    )

def parse_features(element, table):
    features = list()
    while element.name == 'h4':
        if "proficiency" in element.text.lower():
            element = get_next_sibling_by_name(element, 'h4')
        else:
            feature, element = parse_next_feature(element, table)
            features.append(feature)

    return features

NAME_REGEX = re.compile(r"([^()]*) ?\((.*)\)")

text_tags = ["p", "div", "ul"]

def parse_next_feature(element, table):
    match = NAME_REGEX.match(element.text)
    feature_name = None
    feature_type = None
    if match:
        feature_name = match.group(1).strip()
        feature_type = match.group(2)
    else:
        feature_name = element.text.strip()
    element = element.next_sibling

    text = None
    while not element.name or element.name in text_tags:
        if element.name == "p":
            if not text:
                text = element.text
            else:
                text += f"\n{element.text}"
        elif element.name == "ul":
            text += "\n"
            for li in element.findChildren("li"):
                text += f"\n- {li.text}"
            text += "\n"
        element = element.next_sibling

    feature_levels = list()
    feature_level_text = dict()
    current_level = 0
    for row in table:
        current_level += 1
        feat_cell = row.find("td", {"class": "text"})

        for a, split_text in zip(feat_cell.findChildren("a"), feat_cell.text.split(',')):
            if a.text.strip().lower() in feature_name.lower():
                feature_levels.append(current_level)
                if len(split_text.strip()) > len(a.text.strip()):
                    feature_level_text[current_level] = split_text.strip()[len(a.text.strip()):].strip()

    if not feature_levels:
        feature_levels = None

    if not feature_level_text:
        feature_level_text = None

    parts = dict()
    parts['name'] = feature_name
    parts['text'] = text
    if feature_type:
        parts['type'] = feature_type
    if feature_level_text:
        parts['levels_text'] = feature_level_text
    if feature_levels:
        parts['levels'] = feature_levels

    return Feature(**parts), element
