from .item_parser import parse_item
from .class_parser import parse_class
from ..helpers import get_soup

def parse(url, identifier, source):
    soup = get_soup(url)
    if "magic-items" in url:
        return parse_item(soup, identifier, source)
    if "classes" in url:
        return parse_class(soup, identifier, source)
    else:
        raise ValueError("Unrecognized Url")
