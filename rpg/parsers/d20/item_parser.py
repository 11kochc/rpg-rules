import re
from rpg.models import Item, Requirements



COST_REGEX = re.compile(r"Cost *([\d,]*) ?gp" )

def parse_item(soup, identifier, sources):
    article = soup.article
    name = article.h1.text

    lines = article.find_all('p')

    info = parse_info(lines[0])
    lines = lines[1:]

    sections = dict()
    current_section = ""
    for line in lines:
        if 'class' in line.attrs and line.attrs['class'][0] == "divider":
            current_section = line.text
        else:
            if current_section in sections:
                sections[current_section].append(line)
            else:
                sections[current_section] = [line]

    text = ""
    if "DESCRIPTION" in sections:
        text = sections["DESCRIPTION"][0].text
        for line in sections["DESCRIPTION"][1:]:
            text += "\n\n" + line.text

    if "CONSTRUCTION REQUIREMENTS" in sections:
        requirements = sections["CONSTRUCTION REQUIREMENTS"][0]


        req_types = dict()
        for link in requirements.find_all('a'):

            req_type = "special"
            url = link.attrs['href']
            if "feats" in url:
                req_type = "feats"
            elif "spells" in url:
                req_type = "spells"
            req_types[link.text.strip()] = req_type

        (requirements, craft_cost) = requirements.text.rsplit(';', 1)

        for requirement in requirements.split(','):
            requirement = requirement.strip()
            if not requirement in req_types:
                req_types[requirement] = "special"

        matches = COST_REGEX.match(craft_cost.strip())
        craft_cost = matches.group(1).replace(',', '')

        new_dict = invert_dict(req_types)
        new_dict["cost"] = int(craft_cost)

        requirements = Requirements(**new_dict)

    info["id"] = identifier
    info["name"] = name
    info["text"] = text
    info["sources"] = sources
    info["construction_requirements"] = requirements

    return Item(**info)


ITEM_INFO_REGEX = re.compile(r"^(?:(?:"
    r"Aura (?P<aura>[^;]*)|"
    r"Slot (?P<slot>[^;]*)|"
    r"CL (?P<cl>[\d]*)(?:st|nd|rd|th)|"
    r"Weight (?P<weight>[—\d,]*) ?(?:lb.)?|"
    r"Price (?P<price>[\d,]*) gp[^;]*)[; ]*)*"
)

"""Test Strings:
Aura moderate transmutation; CL 8th; Slot belt; Weight 1 lb.; Price 4,000 gp (+2), 16,000 gp (+4), 36,000 gp (+6)
Aura moderate transmutation; CL 8th; Slot headband; Price 4,000 gp (+2), 16,000 gp (+4), 36,000 gp (+6); Weight 1 lb.
"""

def invert_dict(in_dict):
    out_dict = dict()
    for key, value in in_dict.items():
        if not value in out_dict:
            out_dict[value] = list()

        out_dict[value].append(key)
    return out_dict

def parse_info(info_line):
    matches = ITEM_INFO_REGEX.match(info_line.text)
    info = dict()
    info["aura"] = matches.group("aura")
    info["caster_level"] = int(matches.group("cl"))
    info["slot"] = matches.group("slot")
    weight = matches.group("weight").replace(',', '')
    info["weight"] = 0 if weight == "—" else float(weight)
    info["cost"] = int(matches.group("price").replace(',', ''))

    return info
