"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import dataclasses
import os
from typing import get_args, get_origin

import yaml


def _convert_mapping(mapping, key_class, value_class, path):
    converted_mapping = {}
    for key, value in mapping.items():
        converted_key = convert_to_dataclass(key_class, key, path)
        converted_value = convert_to_dataclass(value_class, value, path)
        converted_mapping[converted_key] = converted_value
    return converted_mapping


def convert_to_dataclass(dataclass, value, path):
    """Convert the provided value into the requested dataclass, using a variety of methods based on
    the provided values.

    Args:
        dataclass (type): the type to convert to.
        value: The value to be converted.

    Raises:
        Exception: If no conversion method is found.

    Returns:
        (dataclass): The converted value.
    """

    if get_origin(dataclass) is dict and isinstance(value, dict):
        # If we have a dict[Skill, Feature], we need to convert the keys and values seperately.
        # This flow isn't currently hit, but should still be valid.
        inner_classes = get_args(dataclass)
        key_class = inner_classes[0]
        value_class = inner_classes[1]
        return _convert_mapping(value, key_class, value_class, path)

    if get_origin(dataclass) is list and isinstance(value, list):
        inner_class = get_args(dataclass)[0]
        return list(map(lambda x: convert_to_dataclass(inner_class, x, path), value))

    if isinstance(value, dataclass):
        return value

    if isinstance(value, int) and dataclass == str:
        return str(value)
    if isinstance(value, int) and dataclass == float:
        return value
    if not isinstance(value, dict):
        raise Exception(f'{value} is not a dict or {dataclass.__name__}')

    return dict_to_class(dataclass, value, path)


def read_data(directory: str, name: str) -> dict:
    """Read the data, based on the name, and the location of a file.

    Args:
        directory (str): The directory the file is in.
        name (str): The name of the file, minus the extention (.yml).

    Returns:
        dict: The loaded contents of the file.
    """
    base_data = {}
    if '.' in name:
        base_name = name.rsplit('.', 1)[0]
        base_data = read_data(directory, base_name)

    filename = os.path.join(directory, f'{name}.yml')

    with open(filename, encoding='utf-8') as file:
        data = yaml.load(file, yaml.FullLoader)
    data['id'] = name
    return base_data | data

class ParseError(Exception):
    def __init__(self, path):
        message = f"Failed to parse {path}"
        super().__init__(message)

def dict_to_class(dataclass, mapping: dict, path: str):
    """Converts the provided dictionary, into the dataclass. It finds all keys that match a field
    on the dataclass, and converts each one into the type expected by the dataclass.

    Args:
        dataclass: The class to convert to.
        mapping (dict): The dictionary to convert

    Returns:
        (dataclass): A new instance of the dataclass.
    """
    for field in dataclasses.fields(dataclass):
        if field.name in mapping:
            current_path = path + f".{field.name}"
            try:
                mapping[field.name] = convert_to_dataclass(
                    field.type,
                    mapping[field.name],
                    current_path)
            except Exception as ex:
                raise ParseError(current_path) from ex

    try:
        return dataclass(**mapping)
    except Exception as ex:
        raise ParseError(path) from ex


_READ_DATACLASS_CACHE = dict()

def read_dataclass(dataclass, value, directory=None):
    directory = directory or dataclass.DIRECTORY

    if not isinstance(value, dict):
        if directory in _READ_DATACLASS_CACHE:
            if value in _READ_DATACLASS_CACHE[directory]:
                return _READ_DATACLASS_CACHE[directory][value]
        else:
            _READ_DATACLASS_CACHE[directory] = dict()

    if isinstance(value, dict):
        value = value.copy()
        path = f"{directory}#{value}"
        if 'data_source' in value:
            dict_data = read_data(directory, value.pop('data_source')) | value
        else:
            dict_data = value
    else:
        path = os.path.join(directory, f"{value}.yml")
        path = f'"{path}"'
        dict_data = read_data(directory, value)
        dict_data['id'] = value
    class_data = dict_to_class(dataclass, dict_data, path=path)

    if not isinstance(value, dict):
        # Can't cache if some of the properties are overwritten
        _READ_DATACLASS_CACHE[directory][value] = class_data

    return class_data


def get_cached_dataclasses(dataclass):
    """Returns the entries for the specified dataclass that have already been read.

    Args:
        dataclass (Type): The dataclass to get the types of

    Returns:
        list: A list with all elements of the Dataclass that have been read.
    """
    return _READ_DATACLASS_CACHE[dataclass.DIRECTORY].values()


def read_dir(dataclass, path=None):
    """Read each file from the directory, and create an instance of the dataclass from it.

    This skips over files that have a name that start with '.'

    Args:
        dataclass ([type]): The dataclass to convert to.
        path (str, optional): The path of the directory to read. Defaults to None.

    Returns:
        list: A list of dataclasses, one for each file in the array.
    """
    if path is None:
        path = f'{dataclass.DIRECTORY}'
    results = list()
    for fname in sorted(filter(lambda name: name.endswith('.yml'), os.listdir(path))):
        if fname.startswith('.'):
            # Allows me to comment out files, without losing them.
            continue
        try:
            data = read_dataclass(dataclass, fname[0:-4], path)

            results.append(data)
        except TypeError:
            print(f'unable to convert {fname}')
            raise
    return results
