import argparse
import time

from rpg.generator import generate_site

from jinja2 import Environment, FileSystemLoader, Markup
import markdown

def main(base):
    md = markdown.Markdown(extensions=['meta', 'tables'])
    environment = Environment(loader=FileSystemLoader('template'))
    environment.filters['markdown'] = lambda text: Markup(md.convert(text))
    environment.filters['mod'] = lambda number: str(number) if number <= 0 else '+' + str(number)
    generate_site(environment, base)

if __name__ == "__main__":
    start = time.perf_counter()
    parser = argparse.ArgumentParser(description="Generate the RPG Rules site.")
    parser.add_argument('--base', type=str, default='/')
    args = parser.parse_args()
    main(args.base)
    end = time.perf_counter()

    print(f'Generated site in {end-start} seconds')
