import argparse
import time

from rpg.parsers.d20 import parse
from rpg.pretty_serializer import serialize_dataclass

def main(url, identifier, source):
    data = parse(url, identifier, [source])
    serialize_dataclass(data)


if __name__ == "__main__":
    start = time.perf_counter()
    parser = argparse.ArgumentParser(description="Generate the RPG Rules site.")
    parser.add_argument('url', type=str)
    parser.add_argument('--id', type=str, default='/')
    parser.add_argument('--source', type=str, default='')
    args = parser.parse_args()
    main(args.url, args.id, args.source)
    end = time.perf_counter()

    print(f'Generated site in {end-start} seconds')
